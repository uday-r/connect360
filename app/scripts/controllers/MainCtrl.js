'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the conference360App
 */


angular.module("connect360")
.controller("MainCtrl", 
		[
		"$scope", 
		"$mdMedia", function(
			$scope, 
			$mdMedia) {
	    $scope.$mdMedia = $mdMedia,
    	$scope.showToolbar = false;
	}
]);
