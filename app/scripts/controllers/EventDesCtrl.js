'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:EventDesCtrl
 * @description
 * # EventDesCtrl
 * Controller of the conference360App
 */

angular.module("connect360").controller("EventDesCtrl",
		['$scope'
		, '$routeParams'
		, '$location'
		, '$cookies'
		, 'MomentUtilService'
		, 'EventService'
		, 'EventViewCreateService'
		, '$mdSidenav'
		, '$mdMedia'
		, '$sce'
		, "cloudinary"
			, function(
				$scope
				, $routeParams
				, $location
				, $cookies
				, MomentUtilService
				, EventService
				, EventViewCreateService
				, $mdSidenav
				, $mdMedia
				, $sce
				, cloudinary) {

			$scope.$parent.showToolbar = false;
			$scope.singleDayEvent = false;
			$scope.$mdMedia = $mdMedia;

			$scope.activeMenu = 'info';
			$scope.currentUserIsAdmin = false;

			$scope.loggedInUserIsAdmin = function() {
				if($scope.loggedInUser && $scope.currentEvent.creator == $scope.loggedInUser.user.email) {
					$scope.currentUserIsAdmin = true;
				}
			}

			$scope.createEvent = function() {
				EventViewCreateService.createEvent($scope)
					.then(function(createdEvent) {
					});
			}


			$scope.getLoggedInUser = function() {
				$scope.loggedInUser = $cookies.get('user');
				if($scope.loggedInUser) {
					$scope.loggedInUser = JSON.parse($scope.loggedInUser);
				}
			}

			$scope.logout = function() {
				$scope.loggedInUser = null;
				$cookies.remove('user');
			}

			$scope.login = function() {
				$location.path('/login');
			}

			$scope.toggleSideBar = function() {
				$mdSidenav('left').toggle();
			}

			$scope.trustUrl = function(url) {
				return $sce.trustAsResourceUrl(url);
			}

			$scope.getTransformedImage = function(height, width, publicId) {
				return "http://res.cloudinary.com/" + cloudinary.config().cloud_name + "/image/upload/c_fill/" + $scope.currentEvent.logoUrl + ".jpg"
			}


			var checkIfSingleDayEvent = function() {
				if(moment($scope.currentEvent.start).isSame(moment($scope.currentEvent.end))) {
					$scope.singleDayEvent = true;
				}
			}

			$scope.getCurrentEvent = function() {
                EventService.getAllEvents($routeParams.eventId).then(function(event) {
					$scope.currentEvent = event[0];
					if(!$scope.currentEvent) {
						$location.path('/');
					}
					checkIfSingleDayEvent();
					$scope.loggedInUserIsAdmin();
                }, function(error) {
					$location.path('/');
				});
			}

			$scope.registerEvent = function() {
				$location.path('/event/' + $scope.currentEvent.id + '/schedule');
			}

			$scope.getReadableDate = function(date) {
				return MomentUtilService.getReadableDate(new Date(date));
			}

			$scope.checkIfEventIsOneDay = function() {
				return false;
			}

			$scope.getShortReadableDate = function(date) {
				return MomentUtilService.getShortReadableDate(new Date(date));
			}

			$scope.getCurrentEvent();
			$scope.getLoggedInUser();
}]);

