'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:EventDesCtrl
 * @description
 * # EventDesCtrl
 * Controller of the conference360App
 */

angular.module("connect360").controller("EventScheduleSetupCtrl",
		['$scope'
		, '$routeParams'
		, '$location'
		, '$cookies'
		, '$window'
		, 'MomentUtilService'
		, 'EventService'
		, 'EventViewCreateService'
		, '$mdSidenav'
		, '$mdMedia'
			, function(
				$scope
				, $routeParams
				, $location
				, $cookies
				, $window
				, MomentUtilService
				, EventService
				, EventViewCreateService
				, $mdSidenav
				, $mdMedia) {

			$scope.$parent.showToolbar = false;
			$scope.$mdMedia = $mdMedia;
			$scope.eventDates = [];
			$scope.hours = [
					"09 AM"
					, "10 AM"
					, "11 AM"
					, "12 PM"
					, "01 PM"
					, "02 PM"
					, "03 PM"
					, "04 PM"
					, "05 PM"
					, "06 PM"
				];

			$scope.activeMenu = 'schedule-setup';
			$scope.currentUserIsAdmin = false;

			$scope.toggleSideBar = function() {
				$mdSidenav('left').toggle();
			}

			$scope.createEvent = function() {
				EventViewCreateService.createEvent($scope)
					.then(function(createdEvent) {
					});
			}


			$scope.loggedInUserIsAdmin = function() {
				if(!$scope.loggedInUser || $scope.currentEvent.creator != $scope.loggedInUser.user.email) {
					$window.history.back();
				}
				$scope.currentUserIsAdmin = true;
			}

			$scope.getLoggedInUser = function() {
				$scope.loggedInUser = $cookies.get('user');
				if($scope.loggedInUser) {
					$scope.loggedInUser = JSON.parse($scope.loggedInUser);
				}
			}
            
			$scope.logout = function() {
				$scope.loggedInUser = null;
				$cookies.remove('user');
			}
            
			$scope.login = function() {
				$location.path('/login');
			}

			$scope.getSeatsColor = function(event) {
				if(event.registrationLimit == 0) {
					return;
				}
				var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
				if(seatPercentage < 50) {
					return ' md-accent ';
				} else if(seatPercentage > 50 && seatPercentage < 100) {
					return ' md-warn md-hue-1 ';
				} else if(seatPercentage == 100) {
					return ' md-warn ';
				}
			}


			var getSessionsOn = function(date) {
				var sessionsOnDate = [];
				if($scope.currentEvent.sessions.length == 0) {
					return sessionsOnDate;
				}
				$scope.currentEvent.sessions.every(function(session, sessionIndex) {
					if(moment(session.start).format('DD-MM-YYYY') == moment(date).format('DD-MM-YYYY')) {
						sessionsOnDate.push(session);
					}
					return true;
				});
				return sessionsOnDate;
			}

			var getSessionWithStartTime = function(sessions, hour) {
				var sessionWithGivenTime
				sessions.every(function(session) {
					if(session.start && moment(session.start).format('hh A') == hour) {
						sessionWithGivenTime = session;
						return false
					}
					return true;
				});
				return sessionWithGivenTime;
			}

			var insertDummyTimeSlots = function() {
				var hoursSpan = 0;
				var currentSession;
				for(var datekey in $scope.eventDates) {
					$scope.hours.every(function(hour) {
						currentSession = getSessionWithStartTime($scope.eventDates[datekey]['sessions'], hour);
						if(currentSession == undefined && hoursSpan <= 1) {
							$scope.eventDates[datekey]['sessions'].push(hour);
						} else if(currentSession != undefined) {
							hoursSpan = MomentUtilService.getTimeDifferenceInHours(currentSession.start, currentSession.end);
						} else if(hoursSpan > 1) {
							hoursSpan--;
						}
						return true;
					});
				}
			}

			$scope.removeDuplicateHours = function(newSession) {
				var startTime = moment(newSession.sessionStart);
				var endTime = moment(newSession.sessionEnd);
				var dummyDateIndex;
				var sessionDate = startTime.format('DD-MM-YYYY');
				while(startTime.format('hh:mm A') != endTime.format('hh:mm A')) {
					dummyDateIndex = $scope.eventDates[sessionDate]['sessions'].indexOf(startTime.format('hh A'));
					if(dummyDateIndex != -1) {
						$scope.eventDates[sessionDate]['sessions'].splice(dummyDateIndex, 1);
					}
					startTime.add(1, 'hours');
				}
			}

			var setSessionsDateWise = function() {
				var range = moment().range($scope.currentEvent.start, $scope.currentEvent.end);
				$scope.eventDates = {};
				$scope.eventSpan = range.toArray('days');
				var key;
				$scope.eventSpan.every(function(eventDate) {
					key = moment(eventDate).format('DD-MM-YYYY');
					var sessionDate = {};
					sessionDate.date = eventDate;
					sessionDate.sessions = getSessionsOn(eventDate);
					$scope.eventDates[key] = sessionDate;
					return true;
				});
				insertDummyTimeSlots();
			}

			$scope.getCurrentEvent = function() {
                EventService.getEventWithSessions($routeParams.eventId).then(function(event) {
					$scope.currentEvent = event;
					setSessionsDateWise();
					$scope.loggedInUserIsAdmin();
                }, function(error) {
					$location.path('/');
				});
			}

			$scope.registerEvent = function() {
				EventViewCreateService.registerEvent($scope);
			}

			$scope.addSessionToEvent = function(startTime, sessionDate) {
				$scope.startTime = startTime;
				$scope.sessionDate = moment(sessionDate);
				EventViewCreateService.addSessionToEvent($scope);
			}

			$scope.MomentUtilService = MomentUtilService;

			$scope.getCurrentEvent();
			$scope.getLoggedInUser();
}]);
