angular.module("connect360")
.controller("AddSessionToEventCtrl", [
		'$scope'
		, '$mdDialog'
		, '$mdToast'
		, 'EventService'
		, function(
			$scope
			, $mdDialog
			, $mdToast
			, EventService) {

				$scope.hours = [
					"09 AM"
					, "10 AM"
					, "11 AM"
					, "12 PM"
					, "01 PM"
					, "02 PM"
					, "03 PM"
					, "04 PM"
					, "05 PM"
					, "06 PM"
				];
				$scope.toHours = []

				$scope.getAllUnbookedHours = function() {
					var eventDate = $scope.sessionDate.format('DD-MM-YYYY');
					var allHours = $scope.eventDates[eventDate].sessions;

					$scope.unbookedHours = [];
					allHours.every(function(hour) {
						if(typeof(hour) == "string") {
							$scope.unbookedHours.push(hour);
						}
						return true;
					});
				}
				$scope.getAllUnbookedHours();

				$scope.loadToHours = function() {
					$scope.toHours = [];
					var currentMomentFrom = moment($scope.newSession.fromTime, 'hh A');
					var nextMomentFrom = currentMomentFrom.add(1, 'hours');
					var fromHoursIndex = $scope.unbookedHours.indexOf(nextMomentFrom.format('hh A'));
					$scope.toHours.push(nextMomentFrom.format("hh A"));
					var newIt = true;
					while(fromHoursIndex != -1) {
						if(!newIt) {
							$scope.toHours.push(nextMomentFrom.format("hh A"));
						}
						newIt = false;
						nextMomentFrom = currentMomentFrom.add(1, 'hours');
						fromHoursIndex = $scope.unbookedHours.indexOf(nextMomentFrom.format('hh A'));
					}
					if($scope.toHours.indexOf('06 PM') != -1 && $scope.unbookedHours.indexOf("06 PM")) {
						$scope.toHours.push("07 PM");
					}
				}

				var startTime = moment($scope.startTime, 'hh:mm a');
				$scope.newSession = {
					sessionStartDate: new Date($scope.currentEvent.start),
					sessionDate: new Date($scope.sessionDate),
					sessionEndDate: new Date($scope.currentEvent.end),
					fromTime: startTime.format('hh:mm a'),
					toTime: moment(startTime, 'hh:mm a').hours(startTime.hours() + 1).format('hh:mm a')
				};
				$scope.dateNow = new Date();

				var getSessionIndex = function(sessionStart) {
					var sessionIndex;
					var sessionDate = moment($scope.newSession.sessionStart).format("DD-MM-YYYY")
					var sessions = $scope.eventDates[sessionDate]['sessions']
					sessions.every(function(session, sessionId) {
						if(session == moment(sessionStart).format('hh A')) {
							sessionIndex = sessionId;
							return false;
						}
						return true;
					});
					return sessionIndex;
				}

				$scope.save = function() {
					$scope.newSession.sessionStart = moment($scope.sessionDate).hours(moment($scope.newSession.fromTime, 'HH:SS a').hours()).format();
					$scope.newSession.sessionEnd = moment($scope.sessionDate).hours(moment($scope.newSession.toTime, 'HH:SS a').hours()).format();
					EventService.addSessionToEvent($scope.newSession, $scope.currentEvent.id).then(function(details) {
						$mdToast.show($mdToast.simple().textContent('Succefully added new session named ' + $scope.newSession.sessionName));
						var sessionDate = moment($scope.newSession.sessionStart).format("DD-MM-YYYY")
						var sessionStartTime = moment($scope.newSession.sessionStart).format("hh A")
						$scope.newSession.name = $scope.newSession.sessionName;
						$scope.newSession.start = $scope.newSession.sessionStart;
						$scope.newSession.end = $scope.newSession.sessionEnd;
						$scope.newSession.registrationsDone = 0;
						var sessionIndex = getSessionIndex($scope.newSession.start);
						var sessionSpan = moment($scope.newSession.end).hours() - moment($scope.newSession.start).hours();
						var sessionSpanCounter = 1;
						if(sessionIndex >= 0) {
							$scope.eventDates[sessionDate]['sessions'][sessionIndex] = $scope.newSession;
							while(sessionSpan > 1) {
								$scope.eventDates[sessionDate]['sessions'].splice(sessionIndex + sessionSpanCounter, 1);
								sessionSpanCounter++;
								sessionSpan--;
							}
						}
						$scope.removeDuplicateHours($scope.newSession)
						$mdDialog.hide();
					}, function(err) {
						$mdToast.show($mdToast.simple().textContent('err'));
					});
				}

				$scope.cancel = function() {
					$mdDialog.cancel();
				}
}]);
