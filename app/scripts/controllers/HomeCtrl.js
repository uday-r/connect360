'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the conference360App
 */

angular.module("connect360")
.controller("HomeCtrl", 
	[
		"$scope" 
		, "$location"
		, "$cookies"
		, "EventService"
		, "EventViewCreateService"
		, "MomentUtilService"
		, "$mdMedia"
		, "$mdDialog"
		, "cloudinary"
		, function(
			$scope
			, $location
			, $cookies
			, EventService
			, EventViewCreateService
			, MomentUtilService
			, $mdMedia
			, $mdDialog
			, cloudinary) {

			$scope.$parent.showToolbar = true;
            $scope.events = []; 
			$scope.$mdMedia = $mdMedia;

			$scope.getLoggedInUser = function() {
				$scope.loggedInUser = $cookies.get('user');
				if($scope.loggedInUser) {
					$scope.loggedInUser = JSON.parse($scope.loggedInUser);
				}
			}

			$scope.logout = function() {
				$scope.loggedInUser = null;
				$cookies.remove('user');
			}

			$scope.login = function() {
				$location.path('/login');
			}

			$scope.stopEventDescLoad = function($event) {
				$event.stopPropagation();
			}

			$scope.getReadableDate = function(date) {
				return MomentUtilService.getReadableDate(new Date(date));
			}

			$scope.getShortReadableDate = function(date) {
				return MomentUtilService.getShortReadableDate(new Date(date));
			}

			$scope.createEvent = function() {
				EventViewCreateService.createEvent($scope)
					.then(function(createdEvent) {
					});
			}

			$scope.getCurrentStatus = function(event) {
				if(moment(event.start) < moment()) {
					event.status = "Closed";
				}
				return event.status;
			}

			$scope.getSeatsColor = function(event) {
				if(event.registrationLimit == 0) {
					return;
				}
				var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
				if(seatPercentage < 50) {
					return ' md-accent ';
				} else if(seatPercentage >= 50 && seatPercentage < 100) {
					return ' md-warn md-hue-3 ';
				} else if(seatPercentage == 100) {
					return ' md-warn ';
				}
			}

			$scope.getTransformedImage = function(height, width, publicId) {
				if(publicId) {
					return "http://res.cloudinary.com/" + cloudinary.config().cloud_name + "/image/upload/c_fill/" + publicId + ".jpg";
				} else {
					return "http://res.cloudinary.com/middle-maan/image/upload/v1467122925/connect/BUS-X836E_Overview.jpg"
				}
			}

			$scope.gotoEvent = function(event) {
				$location.path("/event/"+event.id);
			}

            $scope.loadAllEvents = function() {
            	EventService.getAllEvents().then(function(events) {
            		$scope.events = events;
            	}, function(error) {})
            }
			$scope.loadAllEvents();
			$scope.getLoggedInUser();
	}
]);

