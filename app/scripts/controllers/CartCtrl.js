'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the conference360App
 */

angular.module("connect360").controller("CartCtrl",
		['$scope'
		, '$rootScope'
		, '$cookies'
		, '$routeParams'
		, '$location'
		, '$window'
		, 'MomentUtilService'
		, 'EventService'
		, 'EventViewCreateService'
		, '$mdSidenav'
		, '$mdMedia'
			, function(
				$scope
				, $rootScope
				, $cookies
				, $routeParams
				, $location
				, $window
				, MomentUtilService
				, EventService
				, EventViewCreateService
				, $mdSidenav
				, $mdMedia) {
			$scope.$parent.showToolbar = false;
			var selectedSessions = $cookies.get('selectedSessions');
			$scope.selectedSessions = [];
			if(selectedSessions) {
				$scope.selectedSessions = JSON.parse($cookies.get('selectedSessions'));
			}
			$scope.currentEvent = $rootScope.currentEvent;
			if($scope.currentEvent) {
				var currEvent = {'name': $scope.currentEvent.name, 'location': $scope.currentEvent.location, id: $scope.currentEvent.id};
				$cookies.put('currentEvent', JSON.stringify(currEvent));
			} else {
				$scope.currentEvent = JSON.parse($cookies.get('currentEvent'));
			}
			$scope.$mdMedia = $mdMedia;
			$scope.MomentUtilService = MomentUtilService;

			$scope.registerEvent = function() {
				EventViewCreateService.registerToEvent($scope);
			}

			$scope.back = function() {
				$window.history.back();
			}

			$scope.$watch('selectedSessions', function(val) {
				if(!$scope.selectedSessions || $scope.selectedSessions.length == 0) {
					$location.path('/');
				}
			});

			$scope.getSeatsColor = function(event) {
				if(event.registrationLimit == 0) {
					return;
				}
				var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
				if(seatPercentage < 50) {
					return ' md-accent ';
				} else if(seatPercentage >= 50 && seatPercentage < 100) {
					return ' md-warn md-hue-1 ';
				} else if(seatPercentage == 100) {
					return ' md-warn ';
				}
			}

			$scope.removeSession = function(sessionData) {
				var selectedSessionId = -1;
				$scope.selectedSessions.every(function(selectedSession, id) {
					if(selectedSession.name == sessionData.name) {
						selectedSessionId = id;
						return false;
					}
					return true;
				});
				if(selectedSessionId > -1) {
					$scope.selectedSessions.splice(selectedSessionId, 1);
				}
				sessionData.selected = false;
				$cookies.put('selectedSessions', JSON.stringify($scope.selectedSessions));
			}

			$scope.seatsChanged = function(session) {
				if(session.seats != "") {
					session.seats = parseInt(session.seats);
				} else {
					session.seats = 0;
				}
				var remainingSeats = parseInt(session.registrationLimit) - parseInt(session.registrationsDone);
				if(session.seats > remainingSeats) {
					session.seats = remainingSeats;
				}
				session.currentRegistrations = parseInt(session.registrationsDone) + parseInt(session.seats);
			}

}]);
