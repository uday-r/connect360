angular.module("connect360")
.controller("EventCreateCtrl", [
		'$scope'
		, '$mdDialog'
		, '$mdToast'
		, 'EventService'
		, 'Upload'
		, 'cloudinary'
		, function(
			$scope
			, $mdDialog
			, $mdToast
			, EventService
			, $upload
			, cloudinary) {
				$scope.newEvent = {};
				$scope.dateNow = new Date();
				$scope.uploadedImage;
				$scope.uploadStarted = false;
				$scope.tinymceOptions = {
					plugins: 'link code',
					toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
				};

				$scope.save = function() {
					if($scope.newEvent.locationMap.match("iframe")) {
						var iframeHtml = $($scope.newEvent.locationMap);
						if(iframeHtml.attr('src')) {
							$scope.newEvent.locationUrl = iframeHtml.attr('src');
						}
					}
					EventService.createEvent($scope.newEvent).then(function(details) {
						$mdToast.show($mdToast.simple().textContent('Succefully added new event named ' + $scope.newEvent.name));
						$mdDialog.hide();
						$scope.loadAllEvents();
					}, function(err) {
						$mdToast.show($mdToast.simple().textContent('err'));
					});
				}

				$scope.cancel = function() {
					$scope.uploadedImage = null;
					$mdDialog.cancel();
				}


				$scope.uploadFiles = function(files){
					$scope.files = files;
					$scope.uploadStarted = true;
					if (!$scope.files) return;
					angular.forEach(files, function(file){
						if (file && !file.$error) {
							file.upload = $upload.upload({
//<<<<<<< HEAD
								//url: "https://upload.wistia.com/",
								//data: {
									//'file': file,
									//'api_password': '73766d4d93f54f8fe07ae0c045668bd3468a727f5e17f5013324ccc539f7353c'
//=======
								url: "https://api.cloudinary.com/v1_1/" + cloudinary.config().cloud_name + "/upload",
								data: {
									upload_preset: cloudinary.config().upload_preset,
									tags: 'myphotoalbum',
									file: file
//>>>>>>> 4f8f139ef18814d28f0e0c0d9d9176a16cdd0a2e
								}
							}).progress(function (e) {
								$scope.progress = Math.round((e.loaded * 100.0) / e.total);
								file.status = "Uploading... " + file.progress + "%";
							}).success(function (data, status, headers, config) {
								$scope.uploadStarted = false;
								$scope.uploadedImage = data.url;
								$scope.newEvent.logoUrl = data.public_id;
							}).error(function (data, status, headers, config) {
								$scope.uploadStarted = false;
								file.result = data;
							});
						}
					});
				};
}]);
