angular.module("connect360")
.controller("RegisterToEventCtrl", [
		'$scope'
		, '$mdDialog'
		, '$mdToast'
		, '$location'
		, '$timeout'
		, '$cookies'
		, 'EventService'
		, function(
			$scope
			, $mdDialog
			, $mdToast
			, $location
			, $timeout
			, $cookies
			, EventService) {

				$scope.newAttendee = {'sessions': $scope.selectedSessions};
				$scope.hours = [
						"09 AM"
						, "10 AM"
						, "11 AM"
						, "12 PM"
						, "01 PM"
						, "02 PM"
						, "03 PM"
						, "04 PM"
						, "05 PM"
						, "06 PM"
					];

				$scope.save = function() {
					EventService.registerToEvent($scope.newAttendee, $scope.currentEvent.id).then(function(details) {
						$mdToast.show($mdToast.simple().textContent('You have successfully registered, please check your email for further details.'));
						$mdDialog.hide();
						$scope.selectedSessions = [];
						$timeout(function() {
							$location.path('/');
						}, 100);
						$cookies.remove('selectedSessions');
					}, function(err) {
						$mdToast.show($mdToast.simple().textContent('Something went wrong please try again later.'));
					});
				}

				$scope.cancel = function() {
					$mdDialog.cancel();
				}
}]);
