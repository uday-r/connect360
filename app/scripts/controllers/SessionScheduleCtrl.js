'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:SessionScheduleCtrl
 * @description
 * # SessionScheduleCtrl
 * Controller of the conference360App
 */

angular.module("connect360").controller("SessionScheduleCtrl",
		['$scope'
		, '$rootScope'
		, '$routeParams'
		, '$location'
		, '$cookies'
		, 'MomentUtilService'
		, 'EventService'
		, 'EventViewCreateService'
		, '$mdSidenav'
		, '$mdMedia'
			, function(
				$scope
				, $rootScope
				, $routeParams
				, $location
				, $cookies
				, MomentUtilService
				, EventService
				, EventViewCreateService
				, $mdSidenav
				, $mdMedia) {
			$scope.$parent.showToolbar = false;
			$scope.$mdMedia = $mdMedia;
			$scope.eventDates = [];
			$scope.hours = [
					"09 AM"
					, "10 AM"
					, "11 AM"
					, "12 PM"
					, "01 PM"
					, "02 PM"
					, "03 PM"
					, "04 PM"
					, "05 PM"
					, "06 PM"
				];
			var selectedSessions = $cookies.get('selectedSessions');
			$scope.selectedSessions = [];
			if(selectedSessions) {
				$scope.selectedSessions = JSON.parse($cookies.get('selectedSessions'));
			}

			$scope.activeMenu = 'schedule';

			$scope.toggleSideBar = function() {
				$mdSidenav('left').toggle();
			}

			$scope.createEvent = function() {
				EventViewCreateService.createEvent($scope)
					.then(function(createdEvent) {
					});
			}

			
			$scope.currentUserIsAdmin = false;

			$scope.loggedInUserIsAdmin = function() {
				if($scope.loggedInUser && $scope.currentEvent.creator == $scope.loggedInUser.user.email) {
					$scope.currentUserIsAdmin = true;
				}
			}

			$scope.getLoggedInUser = function() {
				$scope.loggedInUser = $cookies.get('user');
				if($scope.loggedInUser) {
					$scope.loggedInUser = JSON.parse($scope.loggedInUser);
				}
			}

			$scope.logout = function() {
				$scope.loggedInUser = null;
				$cookies.remove('user');
			}

			$scope.login = function() {
				$location.path('/login');
			}


			$scope.goToCart = function() {
				$rootScope.selectedSessions = $scope.selectedSessions;
				$rootScope.currentEvent = $scope.currentEvent;
				$cookies.put('selectedSessions', JSON.stringify($scope.selectedSessions));
				$location.path('/cart');
			}

			$scope.removeAllSelectedItems = function() {
				$cookies.remove('selectedSessions');
				var startDate, startTime;
				$scope.selectedSessions.every(function(selectedSession) {
					$rootScope.$emit('removeFromSelectedSessions', selectedSession);
					return true;
				});
				$scope.selectedSessions = [];
			}

			$scope.getSeatsColor = function(event) {
				if(event.registrationLimit == 0) {
					return;
				}
				var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
				if(seatPercentage < 50) {
					return ' md-accent ';
				} else if(seatPercentage > 50 && seatPercentage < 100) {
					return ' md-warn md-hue-1 ';
				} else if(seatPercentage == 100) {
					return ' md-warn ';
				}
			}

			var setSelectedIfSelectedSession = function(session) {
				if($scope.selectedSessions.length == 0) {
					return false;
				}
				$scope.selectedSessions.every(function(selectedSession) {
					if(session.id == selectedSession.id) {
						session.seats = 1;
						session.registrationsDone = selectedSession.registrationsDone;
						session.selected = true;
						return false;
					}
					return true;
				});
			}

			var getSessionsOn = function(date) {
				var sessionsOnDate = [];
				if($scope.currentEvent.sessions.length == 0) {
					return sessionsOnDate;
				}
				$scope.currentEvent.sessions.every(function(session, sessionIndex) {
					setSelectedIfSelectedSession(session);
					if(moment(session.start).format('DD-MM-YYYY') == moment(date).format('DD-MM-YYYY')) {
						sessionsOnDate.push(session);
					}
					return true;
				});
				return sessionsOnDate;
			}

			var setSessionsDateWise = function() {
				var range = moment().range($scope.currentEvent.start, $scope.currentEvent.end);
				$scope.eventDates = {};
				$scope.eventSpan = range.toArray('days');
				var key;
				$scope.eventSpan.every(function(eventDate) {
					key = moment(eventDate).format('DD-MM-YYYY');
					var sessionDate = {};
					sessionDate.date = eventDate;
					sessionDate.sessions = getSessionsOn(eventDate);
					$scope.eventDates[key] = sessionDate;
					return true;
				});
			}

			$scope.getCurrentEvent = function() {
                EventService.getEventWithSessions($routeParams.eventId).then(function(event) {
					$scope.currentEvent = event;
					setSessionsDateWise();
					$scope.loggedInUserIsAdmin();
                }, function(error) {
					$location.path('/');
				});
			}

			$scope.registerEvent = function() {
				EventViewCreateService.registerEvent($scope);
			}

			$scope.addSessionToEvent = function(startTime, sessionDate) {
				$scope.startTime = startTime;
				$scope.sessionDate = moment(sessionDate);
				EventViewCreateService.addSessionToEvent($scope);
			}

			$scope.MomentUtilService = MomentUtilService;

			$scope.getCurrentEvent();
			$scope.getLoggedInUser();
				
}]);
