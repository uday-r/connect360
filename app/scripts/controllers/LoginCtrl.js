'use strict';

/**
 * @ngdoc function
 * @name conference360App.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the conference360App
 */


angular.module("connect360").controller("LoginCtrl", [
		"$scope"
		, "$cookies"
		, "$location"
		, "$window"
		, "$mdMedia"
		, "LoginService"
		, "$mdToast"
		, function(
			$scope
			, $cookies
			, $location
			, $window
			, $mdMedia
			, LoginService
			, $mdToast) {

	$scope.login = function() {
		LoginService.login($scope.username, $scope.password)
			.then(function(userDetails) {
				$cookies.put("user", JSON.stringify(userDetails));
				if($window.history.length > 2) {
					$window.history.go(-1);
				} else {
					$location.path('/');
				}
			},
			function(err) {
				$scope.password = "";
				$mdToast.show($mdToast.simple().textContent("Username or password is incorrect."));
			});
	}

	$scope.routeIfUserLoggedIn = function() {
		var user = $cookies.get("user");
		if(user) {
			if($window.history.length > 2) {
				$window.history.go(-1);
			} else {
				$location.path('/');
			}
		}
	}

	$scope.routeIfUserLoggedIn();
}
]);
