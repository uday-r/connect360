var connect360App = angular.module("connect360");
connect360App.factory("MomentUtilService", ["$http", "$q", function($http, $q) {
	return {
		getReadableDate: function(jsDate) {
			return moment(jsDate).format('ddd, MMM Do YYYY');
		},
		getShortReadableDate: function(jsDate) {
			return moment(jsDate).format('MMM Do');
		},
		getShortDayDate: function(jsDate) {
			return moment(jsDate).format('ddd, MMM Do');
		},
		getNormalDate: function(jsDate) {
			return moment(jsDate).format('DD-MM-YYYY');
		},
		getTime: function(jsDate) {
			return moment(jsDate).format('hh:mm A');
		},
		getTimeDifferenceInHours: function(fromDate, toDate) {
			return moment(toDate).hours() - moment(fromDate).hours();
		}
	}
}]);
