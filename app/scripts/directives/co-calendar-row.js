angular.module("connect360")
.directive('coCalendarRow', function() {
	return {
		restrict: 'E',
		templateUrl: '/views/directives/CalendarRow.html',
		controller: 'CalandarRowCtrl',
		scope: false,
		link: function(scope, ele, attrs) {
			scope.sessionScheduleDate = JSON.parse(attrs.sessionScheduleDate);
			try {
				scope.sessionData = JSON.parse(attrs.sessionData);
			} catch(e) {
				scope.startTime = attrs.sessionData;
			}

		}
	};
});

angular.module("connect360")
.controller('CalandarRowCtrl', 
		[
			'$scope'
			, '$location'
			, '$mdMedia'
			, '$rootScope'
			, function(
				$scope
				, $location
				, $mdMedia
				, $rootScope) {



					$scope.isScheduled = function() {
						if($scope.sessionData == undefined) {
							return false;
						}
						return true;
					}

					$scope.soldOut = function() {
						if($scope.sessionData == undefined) {
							return false;
						}
						if($scope.sessionData.registrationsDone == $scope.sessionData.registrationLimit) {
							return true;
						}
						return false;
					}

					$scope.getSeatsColor = function(event) {
						if(event.registrationLimit == 0) {
							return;
						}
						var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
						if(seatPercentage < 50) {
							return ' md-accent ';
						} else if(seatPercentage >= 50 && seatPercentage < 100) {
							return ' md-warn md-hue-3 ';
						} else if(seatPercentage == 100) {
							return ' md-warn ';
						}
					}


}]);
