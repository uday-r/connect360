angular.module("connect360")
.directive('coSubheader', function() {
	return {
		restrict: 'E',
		templateUrl: '/views/directives/Subheader.html',
		controller: 'SubheaderCtrl',
		link: function(scope, ele, attrs) {
			scope.show = false
			$('#main-container').on('scroll', function(e) {
				if(e.currentTarget.scrollTop >= $('#home').height()) {
					scope.$apply(function() {
						scope.show = true;
					});
				} else {
					scope.$apply(function() {
						scope.show = false;
					});
				}
			});
		}
	};
});

angular.module("connect360")
.controller('SubheaderCtrl', 
		[
			'$mdSidenav'
			, '$scope'
			, '$location'
			, '$mdMedia'
			, function(
				$mdSidenav
				, $scope
				, $location
				, $mdMedia) {

					$scope.$mdMedia = $mdMedia;

}]);
