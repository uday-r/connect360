angular.module("connect360")
.directive('coSidebar', function() {
	return {
		restrict: 'E',
		templateUrl: '/views/directives/Sidebar.html',
		controller: 'SidebarCtrl'
	};
});

angular.module("connect360")
.controller('SidebarCtrl', 
		[
			'$mdSidenav'
			, '$scope'
			, '$location'
			, '$mdMedia'
			, function(
				$mdSidenav
				, $scope
				, $location
				, $mdMedia) {

			$scope.changePath = function(pageName, url) {
				if($scope.activeMenu == pageName) {
					return;
				}
				$location.path('/event/' + $scope.currentEvent.id + '/' + url);
			}

}]);
