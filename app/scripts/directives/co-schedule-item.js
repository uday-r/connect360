angular.module("connect360")
.directive('coScheduleItem', function() {
	return {
		restrict: 'E',
		templateUrl: '/views/directives/ScheduleItem.html',
		controller: 'ScheduleItemCtrl',
		scope: false,
		link: function(scope, ele, attrs) {
			scope.sessionScheduleDate = JSON.parse(attrs.sessionScheduleDate);
			try {
				scope.sessionData = JSON.parse(attrs.sessionData);
				scope.sessionData.seats = 1;
				scope.sessionData.currentRegistrations = scope.session.registrationsDone;
			} catch(e) {
				console.log("Error", e)
			}

		}
	};
});

angular.module("connect360")
.controller('ScheduleItemCtrl', 
		[
			'$scope'
			, '$location'
			, '$mdMedia'
			, '$rootScope'
			, function(
				$scope
				, $location
				, $mdMedia
				, $rootScope) {

					$rootScope.$on('removeFromSelectedSessions', function(event, selectedSession) {
						if(selectedSession.id == $scope.sessionData.id) {
							$scope.sessionData.seats = 0;
							$scope.sessionData.registrationsDone = $scope.sessionData.registrationsDone - 1;
							$scope.sessionData.selected = false;
						}
					});


					//$scope.$watch('sessionData.seats', function(newValue) {
						//if(newValue != "") {
							//newValue = parseInt(newValue);
						//} else {
							//newValue = 0;
						//}
						//var remainingSeats = parseInt($scope.sessionData.registrationLimit) - parseInt($scope.sessionData.registrationsDone);
						//if(newValue > remainingSeats) {
							//$scope.sessionData.seats = remainingSeats;
						//} else {
							//$scope.sessionData.seats = newValue;
						//}
						//$scope.sessionData.currentRegistrations = parseInt($scope.sessionData.registrationsDone) + parseInt($scope.sessionData.seats);
					//});

					$scope.soldOut = function() {
						if($scope.sessionData.registrationsDone == $scope.sessionData.registrationLimit) {
							return true;
						}
						return false;
					}

					$scope.selectSession = function() {
						$scope.sessionData.registrationsDone = $scope.sessionData.registrationsDone + 1;
						$scope.selectedSessions.push($scope.sessionData);
						$scope.sessionData.selected = true;
					}

					$scope.getSeatsColor = function(event) {
						if(event.registrationLimit == 0) {
							return;
						}
						var seatPercentage = (event.registrationsDone*100)/event.registrationLimit;
						if(seatPercentage < 50) {
							return ' md-accent ';
						} else if(seatPercentage >= 50 && seatPercentage < 100) {
							return ' md-warn md-hue-3 ';
						} else if(seatPercentage == 100) {
							return ' md-warn ';
						}
					}

					$scope.removeSession = function() {
						var selectedSessionId = -1;
						$scope.sessionData.registrationsDone = $scope.sessionData.registrationsDone - 1;
						$scope.selectedSessions.every(function(selectedSession, id) {
							if(selectedSession.name == $scope.sessionData.name) {
								selectedSessionId = id;
								return false;
							}
							return true;
						});
						if(selectedSessionId > -1) {
							$scope.selectedSessions.splice(selectedSessionId, 1);
						}
						$scope.sessionData.selected = false;
					}

}]);
