var connect360App = angular.module("connect360");
connect360App.factory("EventViewCreateService", ["$mdDialog", "$mdMedia", "$cookies", function($mdDialog, $mdMedia, $cookies) {
	return {
		createEvent: function(scope) {
			var loggedInUser = $cookies.get('user');
			if(loggedInUser) {
				return $mdDialog.show({
					templateUrl: 'views/addEvent.html',
					controller: 'EventCreateCtrl',
					fullscreen: $mdMedia('xs') || $mdMedia('sm'),
					escapeToClose: false,
					scope: scope,
					preserveScope: true
				});
			}
		},
		addSessionToEvent: function(scope) {
			return $mdDialog.show({
				templateUrl: 'views/addSession.html',
				controller: 'AddSessionToEventCtrl',
				fullscreen: $mdMedia('xs') || $mdMedia('sm'),
				escapeToClose: false,
				scope: scope,
				preserveScope: true
			});
		},
		registerToEvent: function(scope) {
			return $mdDialog.show({
				templateUrl: 'views/registerToEvent.html',
				controller: 'RegisterToEventCtrl',
				fullscreen: $mdMedia('xs') || $mdMedia('sm'),
				escapeToClose: false,
				scope: scope,
				preserveScope: true
			});
		}
	}
}]);
