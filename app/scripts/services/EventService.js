var connect360App = angular.module("connect360");
connect360App.factory("EventService", ["RequestInterceptor", "$q", function(requestInterceptor, $q) {
	return {
		getAllEvents: function(eventId) {
			var deferred = $q.defer();
			var params
			if(eventId) {
				params = {
					'id': eventId
				}
			}
			requestInterceptor.send({
				method: 'GET'
				, 'params': params
				, url: '/getAllEvents'}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});

			return deferred.promise
		},
		getEventWithSessions: function(eventId) {
			var deferred = $q.defer();
			var params
			if(eventId) {
				params = {
					'eventId': eventId
				}
			}
			requestInterceptor.send({
				method: 'GET'
				, 'params': params
				, url: '/getSessionsFor'}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});

			return deferred.promise
		},
		createEvent: function(eventData) {
			var deferred = $q.defer();
			requestInterceptor.send({
				method: "POST",
				url: "/createNewEvent",
				data: eventData}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});
			return deferred.promise
		},
		addSessionToEvent: function(sessionData, eventId) {
			var deferred = $q.defer();
			requestInterceptor.send({
				method: "POST",
				url: "/addSessionToEvent/" + eventId,
				data: sessionData}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});
			return deferred.promise
		},
		registerToEvent: function(attendeeInfo, eventId) {
			var deferred = $q.defer();
			requestInterceptor.send({
				method: "POST",
				url: "/registerUserToEvent/" + eventId,
				data: attendeeInfo}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});
			return deferred.promise
		}
	}
}]);
