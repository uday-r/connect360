var conferenceApp = angular.module('connect360');
conferenceApp.factory('Loading',['$mdDialog', '$mdMedia', function ($mdDialog, $mdMedia) {

    return {
		start: function() {
			$mdDialog.show({
				templateUrl: 'views/loading.html',
			});
		},
		stop: function() {
			$mdDialog.hide();
		}
    };
}]);
