angular.module('connect360').
	factory('RequestInterceptor', ['$http', '$q', '$timeout', '$window', 'Loading', '$cookies',
			function($http, $q, $timeout, $window, loading, $cookies) {
				return {
					send:function(params, isBackgroundRequest) {
						var deferred = $q.defer();

						$timeout(function() {
							var request = {
								method:params.method,
								url:params.url,
								timeout:30000
							};
							if (params.params) {
								request.params = params.params;
							}
							if (params.data) {
								request.data = params.data;
							}
							var loggedInUser = $cookies.get('user');
							if(loggedInUser && request.data) {
								request.data.oauth = JSON.parse(loggedInUser).oauth;
							}
							if(loggedInUser && request.params) {
								request.params.oauth = JSON.parse(loggedInUser).oauth;
							}

							if (params.headers) {
								request.headers = params.headers;
							}
							if (isBackgroundRequest) {
								if (!request.headers) {
									request.headers = {};
								}
							}
							else {
								loading.start()
							}

							$http(request).
								then(function(response, httpStatus) {
									loading.stop()
									deferred.resolve(response.data);
								}, function(err) {
									loading.stop();
									deferred.reject({
									});
								}, function(err) {
									loading.stop();
									deferred.reject({
									});
								});
						});
						return deferred.promise;
					}
				}
			}]);
