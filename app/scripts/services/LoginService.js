var connect360App = angular.module("connect360");
connect360App.factory("LoginService", ["RequestInterceptor", "$q", function(requestInterceptor, $q) {
	return {
		login: function(username, password) {
			var deferred = $q.defer();
			requestInterceptor.send({
				method: "POST",
				url: "/login",
				data: {
					'username': username,
					'password': password
				}}
				, false)
				.then(function(a) {
					deferred.resolve(a)
				}, function(a) {
					deferred.reject(a)
				});
			return deferred.promise
		}
	}
}]);
