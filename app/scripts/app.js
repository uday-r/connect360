'use strict';

/**
 * @ngdoc overview
 * @name connect360
 * @description
 * # conference360App
 *
 * Main module of the application.
 */

angular.module("connect360", 
		["ngAnimate" 
			, "ngAria" 
			, "ngCookies" 
			, "ngMessages" 
			, "ngResource" 
			, "ngRoute"
			, "ngSanitize"
			, "ngMaterial"
			, "ui.tinymce"
			, "cloudinary"
			, "ngFileUpload"])
.config([
		"$routeProvider"
		, "$httpProvider"
		, "$mdThemingProvider"
		, "cloudinaryProvider"
		, function(
			$routeProvider
			, $httpProvider
			, $mdThemingProvider
			, cloudinaryProvider) {
	$routeProvider.when("/", {
		templateUrl: "views/home.html",
		controller: "HomeCtrl",
		controllerAs: "home"
	}).when("/event/:eventId", {
		templateUrl: "views/eventDescription.html",
		controller: "EventDesCtrl"
	}).when("/event/:eventId/schedule-setup", {
		templateUrl: "views/eventScheduleSetup.html",
		controller: "EventScheduleSetupCtrl"
	}).when("/event/:eventId/schedule", {
		templateUrl: "views/sessionSchedule.html",
		controller: "SessionScheduleCtrl"
	}).when("/cart", {
		templateUrl: "views/cart.html",
		controller: "CartCtrl"
	}).when("/login", {
		templateUrl: "views/login.html",
		controller: "LoginCtrl"
	}).otherwise({
		redirectTo: "/"
	})
	cloudinaryProvider.set("cloud_name", 'middle-maan');
	cloudinaryProvider.set("upload_preset", 'vbhyfxos');
	

	$mdThemingProvider.theme('default')
		.primaryPalette('deep-purple', {
			'default': '500',
			'hue-1': '700',
			'hue-2': '300',
			'hue-3': '50'
		}).accentPalette('green', {
			'default': '500',
			'hue-1': '700',
			'hue-2': 'A200'
		}).warnPalette('red', {
			'default': 'A700',
			'hue-1': '500',
			'hue-2': '300',
			'hue-3': '400'
		});

	//$mdThemingProvider.theme('default')
		//.primaryPalette('customPrimary')
		//.accentPalette('orange');
}
]);
