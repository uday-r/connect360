var nforce = require('nforce');
var url = require('url');
var unirest = require('unirest');


defineRoutes = function(app) {
	var org = nforce.createConnection({
		  clientId: '3MVG9ZL0ppGP5UrAb3U7lrEJMRDWEhR9bLe7mx6Biyy7lJiu2g81f1OF3rGhRIsLyy2OQpe13lRF.LiNKUKUL',
		  clientSecret: '5166932735625095190',
		  redirectUri: 'http://localhost:8089/loginSuccess',
		  //apiVersion: 'v27.0',  // optional, defaults to current salesforce API version 
		  //environment: 'production',  // optional, salesforce 'sandbox' or 'production', production default 
		  //mode: 'multi' // optional, 'single' or 'multi' user mode, multi default 
		  autoRefresh: true
	});
	var oauth;
	var userOauth;

	var getAuth = function() {
		org.authenticate({ username: "udayredI@gmail.com", password: "Yadu8147312585"}, function(err, resp){
			if(err) {
				console.log(err);
				return false;
			}
			console.log(resp);
			oauth = resp;
		});
	}
	
	getAuth();
	setInterval(function () {
		getAuth();
	}, 2000000);
	

	app.post('/login', function(req, res) {
		var requestBody = req.body;
		org.authenticate({ username: requestBody.username, password: requestBody.password}, function(err, resp){
			if(err) {
				console.log(err);
				res.status(400).send("Username or password is incorrect");
				return;
			}
			userOauth = resp;
			if(!userOauth) {
				res.status(400).send('Oauth empty');
			}

			unirest.post(userOauth.id)
			.header('Accept', 'application/json')
			.send({ 'access_token': userOauth.access_token })
			.end(function (response) {
				userDetials = response.body;
				res.send({oauth: userOauth, user: userDetials});
			});
		});
	})

	app.post('/getLoggedUserDetails', function(req, res) {
		var requestBody = req.body;
		var userOauth = requestBody.oauth;
		var userDetials;
	});

	app.get('/loginSuccess', function(req, res) {
		console.log("Success login", res);
	});

	var translateSalesForceSessionToJsEvent = function(salesForceObj) {
		var sessionJs = {
			id: salesForceObj.id
		};

		sessionJs.name = salesForceObj.name__c;
		sessionJs.start = salesForceObj.start__c;
		sessionJs.end = salesForceObj.end__c;
		sessionJs.registrationLimit = salesForceObj.registration_limit__c;
		sessionJs.registrationsDone = salesForceObj.registrations_done__c;
		return sessionJs;
	}

	var translateSalesForceEventToJsEvent = function(salesForceObj) {
		console.log("Salesforce object:", salesForceObj);
		var eventJs = {
			id: salesForceObj.id
		};

		eventJs.name = salesForceObj.name__c;
		eventJs.logoUrl = salesForceObj.logo_url__c;

		eventJs.location = salesForceObj.location__c;
		eventJs.locationUrl = salesForceObj.location_url__c;
		eventJs.start = salesForceObj.start__c;
		eventJs.end = salesForceObj.end__c;
		eventJs.details = salesForceObj.details__c;
		eventJs.status = salesForceObj.status__c;
		eventJs.type = salesForceObj.type__c;
		eventJs.registrationLimit = salesForceObj.registration_limit__c;
		eventJs.registrationsDone = salesForceObj.registrations_done__c;
		if(salesForceObj.createdby) {
			eventJs.creator = salesForceObj.createdby.Email;
		}

		return eventJs;

	}

	var searchEvents = function(eventId, res, successCallBack, errorCallback) {
		var selectQuery = "SELECT id, name__c, logo_url__c, location__c, location_url__c, start__c, end__c, details__c, status__C, type__c, registration_limit__c, registrations_done__c, createdBy.email FROM event__c";
		if(eventId) {
			selectQuery = selectQuery + " where id='" + eventId + "'";
		}
		selectQuery = selectQuery + " ORDER BY start__c desc";
		org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
			if(err) {
				errorCallback(resp, err, res);
				return;
			}
			searchedEvents = resp.records;
			successCallBack(searchedEvents, res);
		});
	}

	var searchSessionsByEventId = function(eventId, res, successCallBack, errorCallback) {
		var selectQuery = "SELECT id, name__c, start__c, end__c, registration_limit__c, registrations_done__c FROM session__c";
		if(eventId) {
			selectQuery = selectQuery + " where event_id__c='" + eventId + "'";
		}
		selectQuery = selectQuery + " ORDER BY start__c asc";
		org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
			console.log('Search complete');
			if(err) {
				errorCallback(resp, err, res);
				return;
			}
			searchedEvents = resp.records;
			successCallBack(searchedEvents, res);
		});
	}

	app.get('/getSessionsFor', function(req, res) {
		var params = url.parse(req.url, true).query;

		var jsSessions = [];
		var currentEvent = {};

		searchEvents(params.eventId, res, function(searchedEvents, resp){
			if(searchedEvents.length == 0) {
				res.status(404).send("Event not found")
			}
			currentEvent = translateSalesForceEventToJsEvent(searchedEvents[0]._fields);
			searchSessionsByEventId(params.eventId, res, function(searchedSessions, resp) {
				searchedSessions.every(function(session) {
					jsSessions.push(translateSalesForceSessionToJsEvent(session._fields));
					return true;
				});
				currentEvent.sessions = jsSessions;
				res.send(currentEvent);
			}, function(resp, err, res) {
				if(err.statusCode == 400) {
					console.log(err);
					res.status(404).send("Something went wrong.")
				}
			});
		}, function(resp, err, res) {
			if(err.statusCode == 400) {
				console.log(err);
				res.status(404).send("Event not found")
			}
		});

	});

    app.get('/getAllEvents', function(req, res) {
		var searchedEvents;
		var jsSearchedEvents = [];


		var params = url.parse(req.url, true).query;
		console.log('params', params);
		console.log('params.id', params.id);

		searchEvents(params.id, res, function(searchedEvents, resp){
			console.log('Search complete');
			searchedEvents.every(function(event) {
				jsSearchedEvents.push(translateSalesForceEventToJsEvent(event._fields));
				return true;
			});
			res.send(jsSearchedEvents);
		}, function(resp, err, res) {
			if(err.statusCode == 400) {
				console.log(err);
				res.status(404).send("Event not found")
			}
		});
    });

	app.post('/createNewEvent', function(req, res) {
		console.log(req.body)
		var requestBody = req.body;
		var newEvent = nforce.createSObject('event__c');

		console.log('newEvent id: ', newEvent.id);
		newEvent.set('name__c', requestBody.name);
		newEvent.set('logo_url__c', requestBody.logoUrl);
		newEvent.set('location__c', requestBody.location);
		newEvent.set('location_url__c', requestBody.locationUrl);
		newEvent.set('start__c', new Date(requestBody.eventStartDate));
		newEvent.set('end__c', new Date(requestBody.eventEndDate));
		newEvent.set('details__c', requestBody.details)
		newEvent.set('registration_limit__c', requestBody.registrationLimit)
		newEvent.set('registrations_done__c', requestBody.registrationsDone)
		newEvent.set('type__c', "EVENT")
		newEvent.set('status__c', "Draft")
		
		var userOauth = requestBody.oauth;
		 
		org.insert({ sobject: newEvent, oauth: userOauth }, function(err, resp){
			console.log("error ", err);
			console.log("response ", resp);
			if(!err) {
				searchEvents(resp.id, res, function(searchedEvents, resp){
					resp.send(translateSalesForceEventToJsEvent(searchedEvents[0]));
				});
				return;
			}
			res.status(404).send("Something went wrong.");
		});
	});

	var deleteSession = function(sessionId) {
		var selectQuery = "SELECT id, name__c FROM session__c where id='" + sessionId + "'";

		org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
			if(err) {
				console.log(err);
				res.status(404).send("Limit not woirking.");
			}
			var record = resp.records[0];
			org.delete({ sobject: record, oauth: oauth}, function(err, resp) {
				if(err) {
					console.log('Error', err);
					res.status(404).send("Something went wrong.");
					return;
				}
				res.send("Successfully Rolled back session");
			});
		});
	}

	var updateEventOnAddingSession = function(event, sessionRegDetails, res) {
		console.log(sessionRegDetails);
		event.set("status__c", "Open");
		var currentRegistrationLimit = event._fields.registration_limit__c? parseInt(event._fields.registration_limit__c) + parseInt(sessionRegDetails.registrationLimit): parseInt(sessionRegDetails.registrationLimit);
		var currentRegistrationsDone = event._fields.registrations_done__c? parseInt(event._fields.registrations_done__c) + parseInt(sessionRegDetails.registrationsDone): parseInt(sessionRegDetails.registrationsDone);
		event.set('registration_limit__c', currentRegistrationLimit);
		event.set('registrations_done__c', currentRegistrationsDone);
		console.log("\n\nevent: ", event);
		org.update({ sobject: event, oauth: oauth }, function(err, resp){
			console.log(err);
			if(!err) {
				res.send("Successfully Created session");
				return;
			}
			deleteSession(sessionRegDetails.sessionId);
			res.status(404).send("Something went wrong.");
		});
	}

	app.post('/addSessionToEvent/:eventId', function(req, res) {
		console.log(req.body)

		var eventId = req.params.eventId;

		searchEvents(eventId, res, function(searchedEvents, resp) {
			var requestBody = req.body;
			var newSession = nforce.createSObject('session__c');
			newSession.set('name__c', requestBody.sessionName);
			newSession.set('event_id__c', eventId);
			newSession.set('start__c', new Date(requestBody.sessionStart));
			newSession.set('end__c', new Date(requestBody.sessionEnd));
			newSession.set('registration_limit__c', requestBody.registrationLimit)
			newSession.set('registrations_done__c', 0)
			org.insert({ sobject: newSession, oauth: oauth }, function(err, resp){
				console.log("error ", err);
				console.log("response ", resp);
				if(!err) {
					updateEventOnAddingSession(searchedEvents[0], {
						'registrationLimit': requestBody.registrationLimit,
						'registrationsDone': 0,
						'sessionId': resp.id
					}, res);
					return;
				}
				res.status(404).send("Something went wrong.");
			});
		}, function(resp, err, res) {
			if(err.statusCode == 400) {
				console.log(err);
				res.status(404).send("Event not found")
			}
		});
	});

	var findForSessionById = function(sessions, sessionId) {
		var foundSession;
		sessions.every(function(session) {
			if(sessionId == session.id) {
				foundSession = session;
				return false;
			}
			return true
		});
		return foundSession;
	}

	app.post('/registerUserToEvent/:eventId', function(req, res) {

		var eventId = req.params.eventId;
		var requestBody = req.body;
		var sessions = requestBody.sessions;
		var searchedEvent;
		var searchedSession;
		if(!sessions || sessions.length == 0) {
			res.status(400).send('No sessions found');
		}

		var totalRegistrations = 0;
		sessions.every(function(eventCurrentSession) {
			totalRegistrations = parseInt(totalRegistrations) + parseInt(eventCurrentSession.seats);
			return true;
		});

		//Event update
		var selectQuery = "SELECT id, name__c, logo_url__c, location__c, location_url__c, start__c, end__c, details__c, status__C, type__c, registration_limit__c, registrations_done__c FROM event__c";
		if(eventId) {
			selectQuery = selectQuery + " where id='" + eventId + "'";
		}
		org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
			if(err) {
				errorCallback(resp, err, res);
				return;
			}
			searchedEvent = resp.records[0];
			registerContactToConference(requestBody, searchedEvent._fields.name__c);
			var newRegistrations = parseInt(searchedEvent._fields.registrations_done__c) + totalRegistrations;
			searchedEvent.set('status__c', "Open");
			searchedEvent.set('registrations_done__c', newRegistrations);
			if(newRegistrations == searchedEvent._fields.registration_limit__c) {
				searchedEvent.set('status__c', "Sold out");
			}
			org.update({ sobject: searchedEvent, oauth: oauth}, function(err, resp) {
				if(err) {
					console.log('Error', err);
					res.status(404).send("Something went wrong.");
				}
					//res.send("Successfully updated event");
			});
		});

		sessions.every(function(currentSession) {


			//Session update
			var selectQuery = "SELECT id, name__c, start__c, end__c, registration_limit__c, registrations_done__c FROM session__c";
			selectQuery = selectQuery + " where id='" + currentSession.id + "'";

			org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
				if(err) {
					console.log(err);
					return;
				}
				searchedSession = resp.records[0];
				var currentRegistration = parseInt(searchedSession._fields.registrations_done__c) + parseInt(currentSession.seats);
				searchedSession.set('registrations_done__c', currentRegistration);
				org.update({ sobject: searchedSession, oauth: oauth}, function(err, resp) {
					if(err) {
						console.log('Error', err);
						res.status(404).send("Something went wrong.");
					}
					res.send("Successfully updated Session");
				});
			});

			return true;
		});
	});

	//app.post('/addNewContact', function(req, res) {
	var registerContactToConference = function(registerContact, eventName) {
		var newContact = nforce.createSObject('contact__c');
		newContact.set('title__c', registerContact.firstName + " " + registerContact.lastName);
		newContact.set('email__c', registerContact.email);
		newContact.set('department__c', registerContact.company);
		newContact.set('phone__c', registerContact.phone);

		var sessionDescription = "You have registered for " + eventName + "\n\n Following are the Sessions that you have registered for:\n";
		registerContact.sessions = registerContact.sessions;
		registerContact.sessions.every(function(session) {
			if(new Date(session.start).toDateString() != new Date(session.end).toDateString()) {
				sessionDescription = sessionDescription + session.name + "\n" + "Starts @ " + new Date(session.start).toLocaleString() + " to " + new Date(session.end).toLocaleString() + "\n";
			} else {
				sessionDescription = sessionDescription + session.name + "\n" + "Starts @ " + new Date(session.start).toLocaleString() + " to " + new Date(session.end).toLocaleTimeString() + "\n";
			}
			return true;
		});
		
		newContact.set('description__c', sessionDescription);

		org.insert({ sobject: newContact, oauth: oauth }, function(err, resp){
			console.log("error ", err);
			console.log("response ", resp);
			if(!err) {
				//res.send("Successfully added user.");
			}
		});
	}

	app.get('/deleteEvent', function(req, res) {
		var searchedEvent;

		//var selectQuery = 'SELECT Name FROM Account';
		var selectQuery = 'SELECT id, name__c FROM session__c';
		var params = url.parse(req.url, true).query;

		console.log(params);
		org.query({ query: selectQuery, oauth: oauth}, function(err, resp){
			if(err) {
				res.status(404).send("Limit not woirking.");
			}
			var record = resp.records[0];
			//console.log("\n\n\nURL", record.attributes.url)
			record.set('name__c', "New rec");
			//record.set('id__c', record.attributes.url.split("/")[6]);
			console.log("\n\n\nrecord: ", record);
			//console.log("\n\n\n");
			org.update({ sobject: record, oauth: oauth}, function(err, resp) {
				if(err) {
					console.log('Error', err);
					res.status(404).send("Something went wrong.");
				}
				res.send("Successfully deleted event");
			});
		});
	});

}
